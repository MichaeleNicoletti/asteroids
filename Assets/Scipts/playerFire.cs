﻿using UnityEngine;
using System.Collections;

public class playerFire : MonoBehaviour {
	
	public float timeSinceLastFired = 0.0f;

	public GameObject laser;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		timeSinceLastFired += Time.deltaTime;

		if (timeSinceLastFired > 0.25f) {

			if (Input.GetButton("Fire1")) {
				Instantiate (laser, transform.position, transform.rotation);
				timeSinceLastFired = 0;
			}
					
		}
	}
}
