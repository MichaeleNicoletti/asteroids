﻿using UnityEngine;
using System.Collections;

public class astroidSpawn : MonoBehaviour {

    private Camera gameCam = null;
	private Rect rectCamLocal;
	private Vector3 bottomLeft;
	private Vector3 topRight;
	public GameObject astroid;
	public static int asteroidCount = 0;

	// Use this for initialization
	void Start () {
        gameCam = Camera.main;
	
	}
	
	// Update is called once per frame
	void Update () {
	}

    void LateUpdate()
    {
        Vector3 pos = transform.position;

        rectCamLocal = gameCam.pixelRect;
        bottomLeft = gameCam.ScreenToWorldPoint(new Vector3(rectCamLocal.x, rectCamLocal.y, gameCam.nearClipPlane));
        topRight = gameCam.ScreenToWorldPoint
            (new Vector3(rectCamLocal.x + rectCamLocal.width, rectCamLocal.y + rectCamLocal.height, gameCam.nearClipPlane));
        if (asteroidCount < 4)
        {
            for (; asteroidCount < 4; asteroidCount++)
            {
                Instantiate(astroid,
                    new Vector3(Random.Range(bottomLeft.x, topRight.x), Random.Range(bottomLeft.y, topRight.y), 0), Quaternion.identity);
            }
        }
    }

    void destroyed()
    {
        asteroidCount--;
    }
}
