﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class scoreCounter : MonoBehaviour {

    public static int score;

    public static int finalScore;

    Text text;

    void Awake()
    {
        score = 0;

        text = GetComponent<Text>();
    }

	// Use this for initialization
	void Start () {
	
	} 
	
	// Update is called once per frame
	void Update () {

        text.text = "Score: " + score;

        finalScore = score;
	
	}
}
