﻿using UnityEngine;
using System.Collections;

public class edge : MonoBehaviour {

	private Camera gameCam = null;
	private Rect rectCamLocal;
	private Vector3 bottomLeft;
	private Vector3 topRight;


	// Use this for initialization
	void Start () {
		gameCam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void LateUpdate()
	{
		//Debug.Log (gameCam);

		Vector3 pos = transform.position;
		
		rectCamLocal = gameCam.pixelRect;
		bottomLeft = gameCam.ScreenToWorldPoint (new Vector3 (rectCamLocal.x, rectCamLocal.y, gameCam.nearClipPlane));
		topRight = gameCam.ScreenToWorldPoint
			(new Vector3(rectCamLocal.x+rectCamLocal.width,rectCamLocal.y+rectCamLocal.height, gameCam.nearClipPlane));
		
		//If Player Exits The Right Side
		if(transform.position.x > topRight.x)
		{
			pos.x = bottomLeft.x;
		}
		
		//If Player Exits the Left Side
		if(transform.position.x < bottomLeft.x)
		{
			pos.x = topRight.x;
		}
		
		//If Player Exits the Top
		if(transform.position.y > topRight.y)
		{
			pos.y = bottomLeft.y;
		}
		
		//If Object Exits the bottom of the screen
		if(transform.position.y < bottomLeft.y)
		{
			pos.y = topRight.y;
		}
		
		transform.position = pos;
		
		
	}
}
