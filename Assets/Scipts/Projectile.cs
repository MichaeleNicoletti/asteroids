﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour {

	private int velocity = 500;
	public GameObject player;
	// Use this for initialization


	void Start () {
		Destroy (gameObject, 3);
		Vector2 force = transform.up * velocity;
		GetComponent<Rigidbody2D>().AddForce (force);
	}
	
	// Update is called once per frame
	//Fired once per second
	void Update () {
		}

	void OnTriggerEnter2D(Collider2D collider)
    {
		if (collider.gameObject.name != player.name && collider.gameObject.name != player.name + "(Clone)")
        {
            Destroy(collider.gameObject);
            Destroy(gameObject);
        }
	}
}
