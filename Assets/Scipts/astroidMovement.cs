﻿using UnityEngine;
using System.Collections;

public class astroidMovement : MonoBehaviour {

	public int velocity = 100;

	// Use this for initialization
	void Start () {
		//transform.position += new Vector3(Random.Range (-10, 10), Random.Range (-10, 10), 0);
		transform.Rotate (0, 0, Random.Range (0, 360));
		Vector2 force = transform.up * velocity;
		GetComponent<Rigidbody2D>().AddForce (force);
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
