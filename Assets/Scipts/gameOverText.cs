﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class gameOverText : MonoBehaviour {

    private int finalScore;

    Text text;

	// Use this for initialization
	void Start () {

        text = GetComponent<Text>();
	
	}
	
	// Update is called once per frame
	void Update () {

        finalScore = scoreCounter.finalScore;

        text.text = "Final Score: " + finalScore;	
	}
}
