﻿	using UnityEngine;
using System.Collections;

public class playerInput : MonoBehaviour {

	Rigidbody2D rigidBody;
	public int velocity = 0;
	public int velocityRotation = 0;
	// Use this for initialization
	void Start () {
		rigidBody = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update () {

		Vector2 force = transform.up * velocity;

		if (Input.GetKey ("w"))
		{
			rigidBody.AddForce (force);
		}
		if (Input.GetKey ("a"))
						transform.Rotate (Vector3.forward * velocityRotation);
		if (Input.GetKey ("d"))
						transform.Rotate (Vector3.forward * -velocityRotation);
	}
}
