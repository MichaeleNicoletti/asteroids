﻿using UnityEngine;
using System.Collections;

public class playerDestroy : MonoBehaviour {

	public GameObject player;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.gameObject.name == player.name || collider.gameObject.name == player.name + "(Clone)")
		{
			Destroy (collider.gameObject);

            if (playerLives.lives != 0)
            {
                playerLives.lives--;
                Debug.Log(playerLives.lives);
                Instantiate(player);
            }      
			else if( playerLives.lives == 0)
            {
                Application.LoadLevel(2);
            }
		}
					
	}
}